var gulp = require('gulp'),
	concat = require('gulp-concat'),
	stylus = require('gulp-stylus'),
	minifyCss = require('gulp-clean-css'),
	mainBowerFiles= require('main-bower-files'),
	connect = require('gulp-connect'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	flatten = require('gulp-flatten'),
	order = require('gulp-order'),
	gulpFilter = require('gulp-filter'),
	clean = require('gulp-clean'),
	copy = require('gulp-copy'),
	nib = require('nib'),
	autoprefixer = require('autoprefixer-core'),
	postcss = require('gulp-postcss');

gulp.task('connect', function(){
	return connect.server({
		'port': 1488,
		'livereload': true,
		'root': 'dist'
	});
});

gulp.task('bowerInit', function () {
	var jsFilter = gulpFilter('*.js');
	var cssFilter = gulpFilter('*.css');
	var fontFilter = gulpFilter(['*.eot', '*.woff', '*.svg', '*.ttf', '*.woff2']);
	return gulp.src(mainBowerFiles())
	// grab vendor js files from bower_components, minify and push in /public
	.pipe(jsFilter)
	.pipe(concat('vendor.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('dist/js'))
	.pipe(jsFilter.restore())

	// grab vendor css files from bower_components, minify and push in /public
	.pipe(cssFilter)
	.pipe(minifyCss())
	.pipe(concat('vendor.min.css'))
	.pipe(gulp.dest('dist/css'))
	.pipe(cssFilter.restore())

	// // grab vendor font files from bower_components and push in /public
	.pipe(fontFilter)
	.pipe(flatten())
	.pipe(gulp.dest('dist/fonts'));
});

//Стили
gulp.task('css', function () {
	return gulp.src('app/styl/build.styl')
		.pipe(stylus({ use: nib(), compress: true }))
		.pipe(gulp.dest('dist/css'));
});
gulp.task('cssProduction', ['css'], function () {
	var processors = [
		autoprefixer({browsers: ['ie > 8']})
	];
	return gulp.src(['dist/css/*.css', '!dist/css/main.min.css'])
		.pipe(order(['vendor.min.css','app.min.css']))
		.pipe(concat('main.min.css'))
		.pipe(postcss(processors))
		.pipe(gulp.dest('dist/css'))
		.pipe(connect.reload());
});

//JS
gulp.task('jsSave', function () {
	return gulp.src(['app/js/**/*.js'])
		//.pipe(uglify())
		.pipe(concat('app.min.js'))
		.pipe(gulp.dest('dist/js'));
});
gulp.task('jsProduction', ['jsSave'], function () {
	return gulp.src(['dist/js/*.js', '!dist/js/main.min.js'])
		.pipe(order(['vendor.min.js', 'app.min.js']))
		.pipe(concat('main.min.js'))
		.pipe(gulp.dest('dist/js'))
		.pipe(connect.reload());
});


//Html
gulp.task('html', function () {
	return gulp.src(["app/**/*.html"])
		.pipe(gulp.dest('dist/'))
		.pipe(connect.reload());
});

//Copy img and fonts
gulp.task('copyImg', function () {
	return gulp.src([
						"app/img/**/**"
					])
		.pipe(copy('dist/img', {prefix: 999}));
});

gulp.task('default', ['cssProduction', 'jsProduction', 'copyImg', 'html','connect', 'watch']);

gulp.task('watch', function(){
	gulp.watch('app/**/*.styl', ['cssProduction']);
	gulp.watch('app/js/**/*.js', ['jsProduction']);
	gulp.watch('app/**/*.html', ['html']);
});
