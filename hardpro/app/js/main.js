$(document).ready(function() {
  $('img').dense();
  //Калькулятрор
  var inputs = $('.calc__input'),
      total = $('.calc__total__price'),
      totalWrap = $('.calc__total'),
      spec = $('.calc__spec'),
      maxValue = +$('.calc').data('max'),
      summ = 0;
  inputs.inputmask({
    "mask": "99",
    "rightAlign":false,
    "placeholder": ""
  });
  var calculate = function() {
    summ = 0;
    inputs.each(function(index, el) {
      if($(this).val()){
        summ += $(this).val() * $(this).data('index');
      }
    });
    if(summ > maxValue){
      totalWrap.stop().fadeOut('fast', function(){
        spec.stop().fadeIn('fast');
      });
    }else{
      total.html(summ);
      spec.stop().fadeOut('fast', function(){
        totalWrap.stop().fadeIn('fast');
      });
    }
  };
  calculate();
  inputs.each(function(index, el) {
    $(this).on('keyup change', function(event) {
      summ = 0;
      inputs.each(function(index, el) {
        if($(this).val()){
          summ += $(this).val() * $(this).data('index');
        }
      });
      if(summ > maxValue){
        totalWrap.stop().fadeOut('fast', function(){
          spec.stop().fadeIn('fast');
        });
      }else{
        total.html(summ);
        spec.stop().fadeOut('fast', function(){
          totalWrap.stop().fadeIn('fast');
        });
      }
    });
  });
  $('.calc__controll').click(function(event) {
    var input = $(this).closest('.calc__input__wrap').find('.calc__input'),
        val = +input.val();
    if(!Number.isInteger(val)) return;
    if($(this).is('.calc__controll_up')){
      if(val > 98) return;
      input.val(+input.val()+1).change();
    }else{
      if(val < 1) return;
      input.val(+input.val()-1).change();
    }
  });

  //Попап
  $('.btn_popup').click(function(event) {
    var hiddenInputsDiv = $('.popup__inputs');
    hiddenInputsDiv.html('');

    var thisData = $(this).data();
    if(!thisData.theme) return;
    var input = $('<input type="hidden" >');
    var hiddenInputs = '';
    input.attr({
      name : 'theme',
      value: thisData.theme
    });
    hiddenInputs += input[0].outerHTML;
    if(thisData.calc){
      var total = [];
      inputs.each(function(index, el) {
        if(!$(this).data('title')) return;
        var thisTotal = {};
        thisTotal.title = $(this).data('title');
        thisTotal.val = $(this).val();
        thisTotal.name = $(this).attr('name');
        total.push(thisTotal);
      });
      input.attr({
        name : 'calc',
        value: JSON.stringify(total)
      });
      hiddenInputs += input[0].outerHTML;
    }
    hiddenInputsDiv.html(hiddenInputs);
    $('#popup').find('.form__done').hide();
    $.magnificPopup.open({
      items: {
        src: '#popup'
      },
      type: 'inline',
      preloader: false,
  		midClick: true,
  		removalDelay: 300,
  		mainClass: 'my-mfp-slide-bottom'
    });
  });

  $.validator.addMethod("regx", function(value, element, regexpr) {
    return regexpr.test(value);
  }, "Please enter a valid pasword.");
  $('input[name="phone"]').inputmask("8 (999) 99-99-999");
  $('form').each(function(index, el) {
    $(el).validate({
      errorPlacement: function(error, element) {},
      success: function (label, el) {
        $(el).addClass('success');
      },
      highlight: function (el, errorClass) {
        $(el).removeClass('success').addClass(errorClass);
      },
      unhighlight: function (el, errorClass) {
        $(el).removeClass(errorClass);
      },
      rules: {
        email: {
          email: true
        },
        name: {
          minlength: 2
        },
        phone: {
          regx: /^8 \(\d{3}\) (\d{2}-){2}\d{3}$/
        }
      },
      submitHandler: function (form) {
        var allData = $(form).serialize(),
            button = $(form).find('.btn');
            button.attr({
              disabled: true
            }).text('Отправляем...');
        $.ajax({
          url: 'send.php',
          type: 'POST',
          dataType: 'json',
          data: allData
        }).done(function(resp) {
          if(resp){
            $(form).find('.form__done').fadeIn('fast', function () {
              button.text(button.data('text'));
              $(form).find('.success').removeClass('success');
              $(form).find('.error').removeClass('error');
              $(form)[0].reset();
            });
          }else{
            button.text('Ошибка!');
          }
        }).fail(function() {
          button.text('Ошибка!');
        }).always(function() {
          button.removeAttr('disabled');
        });
      }
    });
  });

  $('.nav__link[href^="#"]').click(function(event) {
  	var target = $(this).attr('href');
  	$('html, body').animate({scrollTop: $(target).offset().top}, 300);
    return false;
  });
});
