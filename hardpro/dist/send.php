<?php

$to = "contact@hardproit.ru";
$subject = "Заявка с сайта";
$headers = ""; 
$headers .= "MIME-Version: 1.0 \r\n"; 
$headers .= "Content-type: text/plain; charset=utf-8 \r\n";
$headers .= "Subject: Заявка с сайта \r\n"; 
$headers .= "X-Mailer: PHP/".phpversion()."\r\n"; 
$phone = null;
$name = null;
$email = null;
$calc = null;
if(isset($_POST['phone'])){ $phone=$_POST['phone']; }
if(isset($_POST['theme'])){ $theme=$_POST['theme']; }
if(isset($_POST['name'])){ $name=$_POST['name']; }
if(isset($_POST['email'])){ $email=$_POST['email']; }
if(isset($_POST['calc'])){ $calc=$_POST['calc']; }
function clearString($string){
	return htmlspecialchars(stripslashes($string));
}
$name = clearString($name);
$phone = clearString($phone);
$email = clearString($email);
$theme = clearString($theme);


$message = "";
if($theme){
	$message .= "Тема: " . $theme . ".\r\n";
}
if($name){
	$message .= "Имя клиента: " . $name . ".\r\n";
}
if($phone){
	$message .= "Телефон клиента: " . $phone . ".\r\n";
}
if($email){
	$message .= "E-mail клиента: " . $email . ".\r\n";
}
if($calc){
	$message .= "Калькулятор::\r\n";
	$calc = json_decode($calc, true);
	foreach ($calc as $value) {
		$message .= $value["title"] . ": " . $value["val"] . ".\r\n";
	}
}


if(mail($to, $subject, $message, $headers)){
	echo 'true';
}else{
	echo 'false';
}
